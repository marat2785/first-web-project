Array.prototype.FindMax = function () {

    let max = 0;

    for (let i = 0; i < this.length; i++) {
        max = max <= this[i] ? this[i] : max;
    }

    return max;
}