import {Tasks} from "./tasks.js";
import {} from "./arrayExtensions.js";

window.onload = function () {
    const closeButton = document.getElementById("closeButton");
    const searchInput = document.getElementById("searchInput");
    const searchButton = document.getElementById("searchButton");

    searchButton.onclick = function () {
        let textContent = searchInput.value;
        switch (textContent) {
            case "google":
                alert("Yandex - лучше! Но это не точно!")
                break
            case "":
                alert("Введите ну хоть что нибудь для поиска")
                break
            default:
                setTimeout(() => alert(textContent), 3000);
        }

        console.log(`Создать массив и вывести имя первого элемента в массиве. Ответ: ${Tasks.GetFirstNameByHumanArray()}`);

        const x = 10;
        const y = 20;
        console.log(`Сложить 2 переменные например x и y. Результат: ${x} + ${y} = ${Tasks.SuperSum(x, y)}`);

        const numbers = Tasks.GetMaxAndMinSumFromArray();
        console.log(`Вывести максимальную и минимальную величины их массива: Ответ: min:${numbers[0]}. max:${numbers[1]}`);

        let a = 'AAA';
        let b = 'BBB';
        const result = Tasks.CreateTwoMemberAndReplaceValues(a,b);
        console.log(`В двух переменных по менять значения местами. Было: ${a} и ${b}. Стало: ${result[0]} и ${result[1]}`)

        const array = [1,2,3,4,5,6,7,8,9];
        console.log(`Вывести максимальное значение массива. (сделал метод расширения): ${array} = ${array.FindMax()}`);

    }

    closeButton.onclick = function () {
        searchInput.value = "";
    }
}