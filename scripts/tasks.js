import {Human} from "./human.js";

export class Tasks {
    static GetFirstNameByHumanArray(){
        let humans = [
            new Human("Марат", 34),
            new Human("Сергей", 77),
            new Human("Наталья", 21),
            new Human("Кирилл", 10)
        ]

        return humans[0].getHumanName();
    }

    static SuperSum(a, b){
        return a+b;
    }

    static GetMaxAndMinSumFromArray(){

        let array = this._getRandomIntArray();
        let min = array[0],
            max = array[0]

        for (let i = 1; i < array.length; i++) {
            let value = array[i]
            min = value < min
                ? value
                : min

            max = value > max
                ? value
                : max
        }

        return [min, max]
    }

    static CreateTwoMemberAndReplaceValues(a, b){
        const tempA = a;
        const tempB = b;
        a = tempB;
        b = tempA;

        return[a, b]
    }

    static _getRandomIntArray(){

        function IntRandom(min, max){
            return min + Math.floor((max - min) * Math.random());
        }

        let intArray = [];
        for(let i = 0; i < 10; i++){
            intArray.push(IntRandom(1, 100));
        }

        return intArray;
    }
}